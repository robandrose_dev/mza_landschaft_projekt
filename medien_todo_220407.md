# Medien TODO

Stand 7.4.2022

[TOC]



## Stationen:



### Prolog

#### Material

* [x] Boxen
* [x] Audiokabel, Stromkable
* [x] Audioplayer, Netzteil, SD-Karte

#### Arbeiten

- [ ] Dauerloop script auf player einrichten mit Testfile



### Passarelle

#### Material

- [ ] 3 Raspberrypies v4 mit Netzteil 
- [ ] Switch mit Netzteil
- [ ] 3 Netzwerkkabel

#### Arbeit

- [ ] Webplayer einrichten
- [ ] Anbindung mit Netzwerk testen



### Landschaft als Kulisse (Rheinfall)

#### Material

- [ ] Ipad mit Kiosk App
- [ ] Netzwerkadapter



### In Landschaften unterwegs (Aussichtspunkt)

#### Material

- [ ] Ipad mit Kiosk App
- [ ] Netzwerkadapter



### Inszenierte Landschaften (Vermittlung)

#### Material

- [ ] Ipad mit Kiosk App
- [ ] Netzwerkadapter



### Landschaftsdefinitionen 3 Stationen

#### Material

- [ ] 6 Einohrhörer
- [ ] 3 Kopfhörerverstärker
- [ ] 3 Audio-player



### Brachengeflüster

- [ ] Ipad mit Kiosk app
- [ ] Netzwerkadapter
- [ ] Aktivbox Boombox



### Videoinstallation

#### Material

- [ ] 2 Raspberryies v4 mit Neztteil
- [ ] 3 Videoplayer Brightsign
- [ ] Switch für Sync, Netzwerkkabel 
- [ ] 4 Beamer Canon
- [ ] 1 Boombox für Master
- [ ] 1 Paar Aktivboxen
- [ ] 1 Audioplayer





## Fragen an Medienmenschen (Michael Kuhn)

* Ipads: Kiosk App vorhanden
* Netzwerk möglich mit Subswitch
* 
