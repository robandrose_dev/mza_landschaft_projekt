# Arbeiten Interaktion MASHLS

Stand 21. Januar 2022



[TOC]

## Meta

* Abklären WLAN, evtl eigenes WLAN besorgen

* Alle Medienplayer und Kopfhörer zusammensuchen
* Anzahl bestimmen



## Webseite

### Eingabeelemente/Ausgabeelemente

#### Brache/Hochsitz

* Eingabe mit Audio
* Ausgabe, Audioplayer mit automatisierter Ausgabe

#### Rheinfall

* Eingabe: Bild und Textdaten
* Ausgabe: Bildloop mit automatisierter Ausgabe

#### Fenstertriptichon

* Eingabe: 3 Bilder und Textdaten
* Ausgabe: Bildloop über 3 Screens, automatisierter Durchlauf

#### Panoramaweg

* Eingabe: Formular mit verschiedenen Eingabeelementen, keine Medien
* Ausgabe: IPad, scrollseite



## Medieninstallationen

### Prolog

* Audioplayer brightsign in Loop

### Passarelle

* 3 Medienplayer / 1 Medienplayer/Macmini für 3 parallele Ausgänge
* 3 Screens (aus Museum)
* Strom, Netzwerk

### Rheinfall

* Ipad / Screen mit Neztwerkmedienplayer
* Strom, Netzwerk

### Panoramaweg

* Ipad
* Strom, Netzwerk

### Hochsitz

* Raspberrypi mit Node.js und Audio-out
* Aktivboxe
* Strom, Netzwerk

### Landschaftsdefinitionen

* 3 Audioplayer Brightsign
* 6 Einohrhörer
* Kopfhörerverstärker Behringer?
* Strom

### Videoinstallation

* Medienplayer mit VLC Player für livestream
* 2 Brightsignplayer in Sync (inkl Netzwerk etc)
* Evtl plus ein Screen 


